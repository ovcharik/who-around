namespace :data do
  
  desc "Users generator"
  task users: :environment do
    count = ENV['count'] || 50
    count = count.to_i
    count.times do
      u = FactoryGirl.create :user
      u.save
      puts u.as_json
    end
  end
  
  desc "Events generator"
  task events: :environment do
    count = ENV['count'] || 100
    count = count.to_i
    count.times do
      e = FactoryGirl.create :event
      e.save
      puts e.as_json
    end
  end
  
  desc "Posts generator"
  task posts: :environment do
    count = ENV['count'] || 1000
    count = count.to_i
    count.times do
      p = FactoryGirl.create :post
      p.save
      puts p.as_json
    end
  end
  
  desc "Links generator"
  task links: :environment do
    count = User.count
    User.all.each do |user|
      rand(3..(count / 3)).to_i.times do
        user.subscribe! FactoryGirl.generate(:random_user)
      end
      puts user.as_json
      puts user.subscriber_ids
    end
  end
  
  desc "Links events generator"
  task post_event: :environment do
    count = Event.where( :event => nil ).count
    Event.all.each do |event|
      rand(0..(count / 3)).to_i.times do
        post = FactoryGirl.generate(:random_post)
        if post
          post.event = event
          post.save
        end
      end
      puts event.as_json
      puts event.posts
    end
  end
  
  desc "All generator"
  task all: :environment do
    Rake::Task["data:users"].invoke
    Rake::Task["data:links"].invoke
    Rake::Task["data:events"].invoke
    Rake::Task["data:posts"].invoke
    Rake::Task["data:post_event"].invoke
  end
  
  desc "All generator"
  task admin_user: :environment do
    u = User.create :username => 'admin',
                :email => 'admin@example.com',
                :password => 'password',
                :password_confirmation => 'password'
    puts u.as_json
  end
  
end
