module GeoQueries
  
  def self.included(base)
    class << base
      EARTH_RADIUS = 6371000.0
      
      def bounds(bounds)
        where( :location => {
          '$within' => {
            '$box' => [
              bounds[:sw].map(&:to_f),
              bounds[:ne].map(&:to_f)
            ]
          }
        } )
      end
      
      def near(location, radius=2000)
        where( :location => {
          '$nearSphere'  => location.map(&:to_f),
          '$maxDistance' => radius.to_f / EARTH_RADIUS
        } )
      end
    end
  end
  
end
