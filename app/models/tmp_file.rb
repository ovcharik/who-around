class TmpFile
  extend Dragonfly::Model
  
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :file_uid, type: String
  dragonfly_accessor :file
end
