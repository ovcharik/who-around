class User
  extend Dragonfly::Model
  
  include Mongoid::Document
  include Mongoid::Timestamps
  
  # counters
  field :subscriber_count
  field :publisher_count
  
  field :subscribed_event_count
  field :published_event_count
  
  field :post_count
  field :event_count
  
  before_save :update_counters
  def update_counters
    self.subscriber_count = self.subscribers.size
    self.publisher_count  = self.publishers.size
    
    self.subscribed_event_count = self.subscribed_events.size
    self.published_event_count  = self.published_events.size
    
    self.post_count  = self.posts.size
    self.event_count = self.events.size
  end
  
  # links
  has_and_belongs_to_many :subscribers,
                          :class_name => 'User',
                          :inverse_of => 'publishers'
  
  has_and_belongs_to_many :publishers,
                          :class_name => 'User',
                          :inverse_of => 'subscribers'
  
  def subscribe!(user)
    unless self.subscriber_ids.include? user.id
      self.subscribers << user
      self.save
      user.save
      return true
    end
    false
  end
  
  def unsubscribe!(user)
    puts self.subscriber_ids, '', user.id
    if self.subscriber_ids.include? user.id
      self.subscribers.delete user
      self.save
      user.save
      return true
    end
    false
  end
  
  # post
  has_many :posts
  
  # events
  has_many :events, :inverse_of => :creator
  
  has_and_belongs_to_many :subscribed_events,
                          :class_name => 'Event',
                          :inverse_of => :subscribers
  has_and_belongs_to_many :published_events,
                          :class_name => 'Event',
                          :inverse_of => :publishers
  
  # messages
  has_many :from_messages, :class_name => 'Message', :inverse_of => 'from'
  has_many :to_messages,   :class_name => 'Message', :inverse_of => 'to'
  
  scope :sort_by_subscribers_count, -> { desc :subscriber_count }
  scope :sort_by_post_count,        -> { desc :post_count }
  
  scope :most_popular, -> { sort_by_subscribers_count.sort_by_post_count }
  
  # login attribute
  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :username,           type: String, default: ""
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable
  
  # Validation
  validates :username, :presence => true, :uniqueness => { :case_sensitive => false }
  
  # Avatar
  field :avatar_uid, type: String
  dragonfly_accessor :avatar do
    default 'public/default_avatar.jpg'
  end
  
  def avatars
    {
      :small  => avatar_thumb(:small),
      :middle => avatar_thumb(:middle),
      :big    => avatar_thumb(:big)
    }
  end
  
  def avatar_thumb(size=nil)
    case size
      when :small  then avatar.thumb('40x40#').url
      when :middle then avatar.thumb('100x100#').url
      when :big    then avatar.thumb('200x200#').url
      else avatar.thumb('200x200#').url
    end
  end
  
  def as_json(options = {})
    options[:methods] = [:avatars]
    super(options)
  end
  
  # auth
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      self.any_of({ :username =>  /^#{Regexp.escape(login)}$/i }, { :email =>  /^#{Regexp.escape(login)}$/i }).first
    else
      super
    end
  end
end
