class Event
  include Mongoid::Document
  include Mongoid::Timestamps
  
  include GeoQueries
  
  has_many :posts
  belongs_to :creator, :class_name => 'User', :inverse_of => :events
  
  field :title,       :type => String
  field :description, :type => String
  
  field :location, :type => Array
  index({ :location => '2dsphere' })
  
  validates :location, :presence => true
  validates :creator, :presence => true
  validates :title, :presence => true
  
  
  has_and_belongs_to_many :subscribers,
                          :class_name => 'User',
                          :inverse_of => :subscribed_events
  has_and_belongs_to_many :publishers,
                          :class_name => 'User',
                          :inverse_of => :published_events
  
  field :subscriber_count
  field :publisher_count
  field :post_count
  
  before_save :update_counters
  def update_counters
    self.subscriber_count = self.subscribers.size
    self.publisher_count  = self.publishers.size
    
    self.post_count  = self.posts.size
  end
  
  def add_publisher! (user)
    unless self.publisher_ids.include? user.id
      self.publishers << user
      self.save
      user.save
      return true
    end
    false
  end
  
  def subscribe!(user)
    unless self.subscriber_ids.include? user.id
      self.subscribers << user
      self.save
      user.save
      return true
    end
    false
  end
  
  def unsubscribe!(user)
    puts self.subscriber_ids, '', user.id
    if self.subscriber_ids.include? user.id
      self.subscribers.delete user
      self.save
      user.save
      return true
    end
    false
  end
  
end
