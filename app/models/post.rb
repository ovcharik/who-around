class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  
  include GeoQueries
  
  belongs_to :user
  belongs_to :event
  
  field :text, :type => String
  
  field :location, :type => Array
  index({ :location => '2dsphere' })
  
  validates :user, :presence => true
  validates :location, :presence => true
  validates :text, :presence => true
  
  after_save    :update_counter
  after_destroy :update_counter
  after_save    :update_event
  
  def update_counter
    self.user.save
  end
  
  def update_event
    unless self.event_id.nil?
      self.event.add_publisher!(self.user)
    end
  end
  
  def as_json(options = {})
    super(options)
  end
  
end
