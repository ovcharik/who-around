class Message
  include Mongoid::Document
  include Mongoid::Timestamps
  
  belongs_to :from, :class_name => 'User', :inverse_of => 'from_messages'
  belongs_to :to,   :class_name => 'User', :inverse_of => 'to_messages'
end
