class CollectionController < ApplicationController
  
  has_scope :page, :default => 1, :allow_blank => true do |controller, scope, value|
    per = 20
    value = value.to_i
    scope.skip((value - 1) * per).limit(per)
  end
  
end
