class EventsController < ApplicationController
  
  before_action :authenticate_user!, :only => [:read, :unread]
  before_filter :find_event
  
  def show
    @user = @event.creator
    respond_to do |format|
      format.json { render :json => @event }
      format.html { render }
    end
  end
  
  def subscribers
    @users = apply_scopes(@event.subscribers).all
    respond_to do |format|
      format.json { render :json => @users }
      format.html { render }
    end
  end
  
  def publishers
    @users = apply_scopes(@event.publishers).all
    respond_to do |format|
      format.json { render :json => @users }
      format.html { render }
    end
  end
  
  def read
    r = @event.subscribe! current_user
    respond_to do |format|
      format.json { render :json => r, :status => (r ? 200 : 403) }
    end
  end
  
  def unread
    r = @event.unsubscribe! current_user
    respond_to do |format|
      format.json { render :json => r, :status => (r ? 200 : 403) }
    end
  end
  
  protected
  
  def find_event
    @event = Event.where(:id => params[:id] || params[:event_id]).first
  end
end
