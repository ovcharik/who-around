class PostsController < CollectionController
  
  before_action :authenticate_user!, :only => [:feed]
  
  has_scope :user_id do |controller, scope, value|
    scope.where(:user_id => value)
  end
  
  has_scope :event_id do |controller, scope, value|
    scope.where(:event_id => value)
  end
  
  def index
    @posts = apply_scopes(Post).desc(:created_at).all
    respond_to do |format|
      format.json { render :json => @posts }
      format.html { render }
    end
  end

  def show
    @post = apply_scopes(Post).where(:id => params[:id]).first
    respond_to do |format|
      format.json { render :json => @post }
      format.html { render }
    end
  end
  
  def create
    post = params[:post]
    location = post[:location]
    
    ll = [location[:lng].to_f, location[:lat].to_f]
    text = post[:text]
    event = post[:event] || nil
    
    @post = current_user.posts.create(:text => text, :location => ll, :event_id => event)
    puts @post.errors
    respond_to do |format|
      format.json { render :json => @post }
    end
  end
  
  def feed
    @ids = current_user.publisher_ids.to_a
    @ids << current_user.id
    @posts = apply_scopes(Post)
      .any_of({:user_id.in => @ids}, {:event_id.in => current_user.subscribed_event_ids})
      .desc(:created_at)
      .includes([:user, :event])
      .all
    respond_to do |format|
      format.json { render :json => @posts.map{ |p| p.as_json.merge({:user => p.user, :event => p.event}) } }
    end
  end
  
  def event_feed
    @posts = apply_scopes(Post)
      .where(:event_id => params[:event_id])
      .desc(:created_at)
      .includes([:user, :event])
      .all
    respond_to do |format|
      format.json { render :json => @posts.map{ |p| p.as_json.merge({:user => p.user, :event => p.event}) } }
    end
  end
end
