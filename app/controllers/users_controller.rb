class UsersController < CollectionController
  
  before_action :authenticate_user!, :only => [:read, :unread]
  before_filter :find_user, :except => [:index]
  
  def index
    @users = apply_scopes(User).all
    respond_to do |format|
      format.json { render :json => @users }
      format.html { render }
    end
  end

  def show
    respond_to do |format|
      format.json { render :json => @user }
      format.html { render }
    end
  end
  
  def subscribers
    @users = apply_scopes(@user.subscribers).all
    respond_to do |format|
      format.json { render :json => @users }
      format.html { render }
    end
  end
  
  def publishers
    @users = apply_scopes(@user.publishers).all
    respond_to do |format|
      format.json { render :json => @users }
      format.html { render }
    end
  end
  
  def subscribed_events
    @events = apply_scopes(@user.subscribed_events).all
    respond_to do |format|
      format.json { render :json => @events }
      format.html { render }
    end
  end
  
  def published_events
    @events = apply_scopes(@user.published_events).all
    respond_to do |format|
      format.json { render :json => @events }
      format.html { render }
    end
  end
  
  def read
    r = @user.subscribe! current_user
    respond_to do |format|
      format.json { render :json => r, :status => (r ? 200 : 403) }
    end
  end
  
  def unread
    r = @user.unsubscribe! current_user
    respond_to do |format|
      format.json { render :json => r, :status => (r ? 200 : 403) }
    end
  end
  
  protected
  
  def find_user
    @user = User.where(:id => params[:id] || params[:user_id]).first
  end
end
