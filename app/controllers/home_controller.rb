class HomeController < ApplicationController
  
  def index
    if user_signed_in?
      redirect_to :action => :feed
    end
  end
  
  def feed
    unless user_signed_in?
      redirect_to :action => :index
    end
    @user = current_user
  end
  
  def around_posts
    @posts = Post
      .bounds(params[:bounds])
      .desc(:created_at)
      .limit(10)
      .includes([:user, :event])
      .all
    
    respond_to do |format|
      format.json { render :json => @posts.map{ |p| p.as_json.merge({:user => p.user, :event => p.event}) } }
    end
  end
  
  def around_events
    @events = Event
      .near(params[:location], params[:radius])
      .desc(:created_at)
      .limit(9)
      .includes(:creator)
      .all
    
    respond_to do |format|
      format.json { render :json => @events }
    end
  end
  
  def popular_users
    @users = User.most_popular.limit(3).all
    
    respond_to do |format|
      format.json { render :json => @users }
    end
  end
  
end
