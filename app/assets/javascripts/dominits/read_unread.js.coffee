wa.dominits.readUnread = ->
  if wa.global.currentUser
    $('body').on 'click', 'a[wa-action=read]:not(.disabled)', (event) ->
      event.preventDefault()
      id = $(@).attr 'wa-id'
      type = $(@).attr 'wa-type'
      
      $buttons = $("a[wa-action=read][wa-id='#{id}']")
      $buttons.addClass 'disabled'
      
      wa.global.currentUser.read id, type,
        success: (args...) ->
          alertify.success 'Success!'
          
          $buttons.removeClass 'disabled'
          
          $buttons.removeClass 'btn-default'
          $buttons.addClass 'btn-success'
          
          $buttons.attr 'wa-action', 'unread'
          
          $buttons.text 'Unread'
        error: ->
          alertify.error 'Error!'
          $buttons.removeClass 'disabled'
    
    $('body').on 'click', 'a[wa-action=unread]:not(.disabled)', (event) ->
      event.preventDefault()
      id = $(@).attr 'wa-id'
      type = $(@).attr 'wa-type'
      
      $buttons = $("a[wa-action=unread][wa-id='#{id}']")
      $buttons.addClass 'disabled'
      
      wa.global.currentUser.unread id, type,
        success: (args...) ->
          alertify.success 'Success!'
          
          $buttons.removeClass 'disabled'
          
          $buttons.removeClass 'btn-success'
          $buttons.addClass 'btn-default'
          
          $buttons.attr 'wa-action', 'read'
          
          $buttons.text 'Read'
        error: ->
          alertify.error 'Error!'
          $buttons.removeClass 'disabled'
