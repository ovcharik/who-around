wa.dominits.groupedTabs = ->
  $('body').on 'show.bs.tab', "[data-tabs-group]", (e) ->
    $e = $(e.target)
    id = $e.attr 'href'
    group = $e.data 'tabsGroup'
    
    $a = $("[data-tabs-group='#{group}']")
    $a.removeClass 'active'
    $a.parents('li').removeClass 'active'
    
    $a = $("[data-tabs-group='#{group}'][href='#{id}']")
    $a.addClass 'active'
    $a.parents('li').addClass 'active'
