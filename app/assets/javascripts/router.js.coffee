class wa.Router extends Backbone.Router
  
  routes:
    ''    : 'home'
    'feed': 'feed'
    
    'users/:id' : 'user'
    'events/:id': 'event'
    'posts/:id' : 'post'
  
  home: ->
    homePage = new wa.views.HomePage
  
  feed: ->
    feedPage = new wa.views.FeedPage
  
  user: (id) ->
    userPage = new wa.views.UserPage
  
  event: (id) ->
    eventPage = new wa.views.EventPage
    
  post: (id) ->
    console.log 'post', id
