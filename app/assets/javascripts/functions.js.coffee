wa.f.formatBounds = (bounds) ->
  sw = bounds.getSouthWest && bounds.getSouthWest() || bounds[0] || bounds.sw || [null, null]
  ne = bounds.getNorthEast && bounds.getNorthEast() || bounds[1] || bounds.ne || [null, null]
  {
    sw: wa.f.formatLocation(sw)
    ne: wa.f.formatLocation(ne)
  }

wa.f.formatLocation = (ll) ->
  [
    ll[0] || ll.longitude || typeof ll.lng is 'function' && ll.lng() || ll.lat || null
    ll[1] || ll.latitude  || typeof ll.lat is 'function' && ll.lat() || ll.lng || null
  ]

wa.f.render = (tpl, options = {}) ->
  throw 'Required template name' unless tpl
  
  locals = _.extend {}, wa.global, options
  wa.templates[tpl](locals)
