wa.mixins.ScrollListener =
  extended: ->
    @after_init 'initScroll'
    
    @include
      
      initScroll: ->
        @scrollTrigger ||= 300
        @scrollStop    ||= false
        @scrollFunc    ||= =>
          @checkScroll()
        
        @_window = $(window)
        @_window.on 'scroll', @scrollFunc
      
      offScroll: ->
        @scrollFunc? && @_window?.off 'scroll', @scrollFunc
      
      checkScroll: ->
        if not @scrollStop
          wh = @_window.height()
          wo = @_window.scrollTop()
          
          eh = @$el.height() + @$el.offset().top
          
          if (eh - wh - wo) < @scrollTrigger
            @trigger 'scroll'
