wa.mixins.Callbacks =
  create_callback: (name, fname) ->
    @callbacks[name] ||= []
    @callbacks[name].push fname
  
  after_init: (fname) ->
    @create_callback 'after_init', fname
  
  extended: ->
    @callbacks = {}
    oldInitialize = @::initialize
    
    @include
      runCallbacks: (name) ->
        f = @constructor.callbacks[name]
        _.map f, (fname) =>
          @[fname]?.apply(@)
      
      initialize: (args...) ->
        oldInitialize.apply(@, args)
        @runCallbacks 'after_init'
