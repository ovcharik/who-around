class wa.models.Event extends Backbone.RelationalModel
  idAttribute: "_id"
  urlRoot: '/events'
  
  relations: [
    {
      type: Backbone.HasOne
      key: 'creator'
      keySource: 'creator_id'
      relatedModel: 'wa.models.User'
      
      reverseRelation:
        type: Backbone.HasMany
        key: 'events'
        collectionType: 'wa.collections.Events'
        collectionOptions: (creator) ->
          url: '/events'
          defaultParams:
            creator_id: creator.id
    },
    {
      type: Backbone.HasMany,
      key: 'subscribers'
      
      keyDestination: 'subscribed_event_ids'
      keySource     : 'subscriber_ids'
      
      relatedModel:   'wa.models.User'
      
      collectionType: 'wa.collections.Users'
      collectionOptions: (event) ->
        url: '/events/subscribers'
        defaultParams:
          event_id: event.id
    },
    {
      type: Backbone.HasMany,
      key: 'publishers'
      
      keyDestination: 'published_event_ids'
      keySource     : 'publisher_ids'
      
      relatedModel:   'wa.models.User'
      
      collectionType: 'wa.collections.Users'
      collectionOptions: (event) ->
        url: '/events/publishers'
        defaultParams:
          event_id: event.id
    }
  ]
  
  lat: ->
    @get('location')[1]
  
  lng: ->
    @get('location')[0]
  
  latlng: ->
    @ll ||= new google.maps.LatLng(@lat(), @lng())
