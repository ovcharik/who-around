class wa.models.Post extends Backbone.RelationalModel
  idAttribute: "_id"
  urlRoot: '/posts'
  
  relations: [
    {
      type: Backbone.HasOne
      key: 'user'
      keySource: 'user_id'
      relatedModel: 'wa.models.User'
      autofetch: true
      
      reverseRelation:
        type: Backbone.HasMany
        key: 'posts'
        collectionType: 'wa.collections.Posts'
        collectionOptions: (user) ->
          url: '/posts'
          defaultParams:
            user_id: user.id
    }
    {
      type: Backbone.HasOne
      key: 'event'
      keySource: 'event_id'
      relatedModel: 'wa.models.Event'
      
      reverseRelation:
        type: Backbone.HasMany
        key: 'posts'
        collectionType: 'wa.collections.Posts'
        collectionOptions: (event) ->
          url: '/posts'
          defaultParams:
            event_id: event.id
    }
  ]
  
  lat: ->
    @get('location')[1]
  
  lng: ->
    @get('location')[0]
  
  latlng: ->
    @ll ||= new google.maps.LatLng(@lat(), @lng())
