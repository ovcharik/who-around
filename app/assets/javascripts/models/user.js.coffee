class wa.models.User extends Backbone.RelationalModel
  idAttribute: "_id"
  urlRoot: '/users'
  
  relations: [
    {
      type: Backbone.HasMany,
      key: 'subscribers'
      
      keyDestination: 'publisher_ids'
      keySource     : 'subscriber_ids'
      
      relatedModel:   'wa.models.User'
      
      collectionType: 'wa.collections.Users'
      collectionOptions: (user) ->
        url: '/users/subscribers'
        defaultParams:
          user_id: user.id
    },
    {
      type: Backbone.HasMany,
      key: 'publishers'
      
      keyDestination: 'subscriber_ids'
      keySource     : 'publisher_ids'
      
      relatedModel:   'wa.models.User'
      collectionType: 'wa.collections.Users'
      collectionOptions: (user) ->
        url: '/users/publishers'
        defaultParams:
          user_id: user.id
    },
    {
      type: Backbone.HasMany,
      key: 'subscribed_events'
      
      keyDestination: 'subscriber_ids'
      keySource     : 'subscribed_event_ids'
      
      relatedModel:   'wa.models.Event'
      
      collectionType: 'wa.collections.Events'
      collectionOptions: (user) ->
        url: '/users/subscribed_events'
        defaultParams:
          user_id: user.id
    },
    {
      type: Backbone.HasMany,
      key: 'published_events'
      
      keyDestination: 'publisher_ids'
      keySource     : 'published_event_ids'
      
      relatedModel:   'wa.models.Event'
      
      collectionType: 'wa.collections.Events'
      collectionOptions: (user) ->
        url: '/users/published_events'
        defaultParams:
          user_id: user.id
    }
  ]
  
  loadPosts: ->
    @get('posts').load()
  
  subscribed: (user) ->
    keys = @getRelation('publishers').keyContents
    user.id != @id && _.include(keys, user.id)
  
  read: (userId, type, options) ->
    $.ajax _.extend {}, options, {
      url: "/#{type}s/read"
      type: 'GET'
      data:
        user_id: userId
      success: (args...) =>
        #TODO
        if type == 'user'
          keys = @getRelation('publishers').keyContents
          keys.push userId
          options?.success?.apply(args)
    }
  
  unread: (userId, type, options) ->
    $.ajax _.extend {}, options, {
      url: "/#{type}s/unread"
      type: 'GET'
      data:
        user_id: userId
      success: (args...) =>
        #TODO
        if type == 'user'
          keys = @getRelation('publishers').keyContents
          index = keys.indexOf(userId)
          if index > -1
            keys.splice(index, 1)
          options?.success?.apply(args)
    }
  
  createMessage: (text, location, event, options) ->
    posts = @get 'posts'
    
    url = posts.url()
    method = 'POST'
    data =
      post:
        text: text
        location: location
        event: event
    
    $.ajax _.extend {}, options,
      url: url,
      type: method,
      data: data
      success: (args...) ->
        options?.success?.apply(args)
