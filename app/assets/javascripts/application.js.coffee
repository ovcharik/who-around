## Libs ##

#= require jquery
#= require jquery-ui
#= require jquery-form

#= require alertify

#= require bootstrap

#= require underscore
#= require backbone
#= require backbone-relational

#= require websocket_rails/main


## WA ##

#= require ./config
#= require ./module
#= require ./templates
#= require ./functions
#= require ./router

#= require_tree ./dominits
#= require_tree ./mixins
#= require_tree ./base
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views

#= require ./init
