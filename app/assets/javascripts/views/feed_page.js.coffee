class wa.views.FeedPage extends Backbone.View
  
  initialize: ->
    @user = wa.global.currentUser
    @feed = new wa.collections.Posts
      url: '/posts/feed'
    
    @subscribers = @user.get 'subscribers'
    @publishers = @user.get 'publishers'
    
    @postsView = new wa.views.UserTab
      el:        '#posts'
      template:  'post'
      collection: @feed
    
    @subscribersView = new wa.views.UserTab
      el:        '#subscribers'
      template:  'user'
      collection: @subscribers
    
    @publishersView = new wa.views.UserTab
      el:        '#publishers'
      template:  'user'
      collection: @publishers
    
    @map = new wa.views.UserMap
      collection: @feed
