class wa.views.BaseForm extends Backbone.View
  
  events:
    'submit': 'submit'
  
  initialize: ($el, url) ->
    @url = url
    
    @$el = $el
    @delegateEvents()
    
    @action = @$el.attr 'action'
    @method = @$el.attr 'method'
  
  submit: (event) ->
    event.preventDefault()
    
    data = @$el.serialize()
    $.ajax
      url:     @action
      method:  @method
      data:    data
      success: (data) =>
        alertify.success 'Success!'
        window.location = ( typeof @url is 'function' && @url() || @url || window.location.href )
      error:   (errors...) =>
        alertify.error 'Error!'
        console.debug errors
