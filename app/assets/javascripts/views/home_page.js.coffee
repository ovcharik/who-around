class wa.views.HomePage extends Backbone.View
  
  initialize: ->
    map    = new wa.views.HomeMapContainer
    events = new wa.views.HomeEvents
    users  = new wa.views.HomePopularUsers
