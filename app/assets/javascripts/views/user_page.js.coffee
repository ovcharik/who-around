class wa.views.UserPage extends Backbone.View
  
  initialize: ->
    @user = wa.global.user
    @posts = @user.get 'posts'
    @subscribers = @user.get 'subscribers'
    @publishers = @user.get 'publishers'
    
    @postsView = new wa.views.UserTab
      el:        '#posts'
      template:  'post'
      collection: @posts
    
    @subscribersView = new wa.views.UserTab
      el:        '#subscribers'
      template:  'user'
      collection: @subscribers
    
    @publishersView = new wa.views.UserTab
      el:        '#publishers'
      template:  'user'
      collection: @publishers
    
    @map = new wa.views.UserMap
      collection: @posts
