class wa.views.HomeMap extends Backbone.View
  
  delay: 500
  el: '#homeMap'
  
  initialize: (@collection) ->
    @markers = []
    @bindEvents()
  
  bindEvents: ->
    
  
  render: ->
    _.each @markers, (m) -> m.setMap(null)
    @collection.each (m) =>
      @markers.push new google.maps.Marker
        position: m.latlng()
        map: @gmap
  
  createMap: (location) ->
    center = new google.maps.LatLng location.latitude, location.longitude
    @gmap = new google.maps.Map @$el[0],
      center: center
      zoom: 13
    
    google.maps.event.addListener @gmap, 'bounds_changed', @onBoundsChanged
  
  onBoundsChanged: =>
    if @timeoutId
      clearTimeout @timeoutId
      @timeoutId = undefined
    @timeoutId = setTimeout @updateBounds, @delay
  
  updateBounds: =>
    @trigger 'update_bounds', @gmap.getBounds()
