class wa.views.HomeMapContainer extends Backbone.View
  
  el: '#homeMapContainer'
  
  initialize: ->
    @collection = new wa.collections.Posts
      url: '/around_posts'
    @mapView    = new wa.views.HomeMap @collection
    @postsView  = new wa.views.HomeMapPosts @collection
    
    navigator.geolocation.getCurrentPosition (position) =>
      @mapView.createMap position.coords
    
    @bindEvents()
  
  bindEvents: ->
    @mapView.on 'update_bounds', @loadMessages, @
    
    @collection.on 'sync',  @render, @
    @collection.on 'error', @error, @
  
  loadMessages: (bounds) ->
    @collection.load
      bounds: wa.f.formatBounds bounds
  
  render: ->
    @mapView.render()
    @postsView.render()
  
  error: (errors...) ->
    console.error errors
