class wa.views.HomePopularUsers extends Backbone.View
  
  el: '#homePopularContainer'
  template: 'user'
  
  initialize: ->
    @collection = new wa.collections.Users
      url: '/popular_users'
    @bindEvents()
    @collection.load()
  
  bindEvents: ->
    @collection.on 'sync',  @render, @
    @collection.on 'error', @error, @
  
  render: ->
    @$el.html ''
    @collection.each (m) =>
      @$el.append wa.f.render(@template, { model: m })
  
  error: (errors...) ->
    console.error errors
