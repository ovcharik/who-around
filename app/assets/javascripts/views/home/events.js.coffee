class wa.views.HomeEvents extends Backbone.View
  
  el: '#homeEventContainer'
  template: 'event'
  
  initialize: ->
    @collection = new wa.collections.Events
      url: '/around_events'
    @bindEvents()
    
    navigator.geolocation.getCurrentPosition (position) =>
      @collection.load
        location: wa.f.formatLocation position.coords
        radius:   10000
  
  bindEvents: ->
    @collection.on 'sync',  @render, @
    @collection.on 'error', @error, @
  
  render: ->
    @$el.html ''
    @collection.each (m) =>
      @$el.append wa.f.render(@template, { model: m })
  
  error: (errors...) ->
    console.error errors
