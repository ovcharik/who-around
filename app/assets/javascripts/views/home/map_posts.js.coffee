class wa.views.HomeMapPosts extends Backbone.View
  
  el: '#homeMapPosts'
  template: 'post'
  
  initialize: (@collection) ->
    @bindEvents()
  
  bindEvents: ->
    
  render: ->
    @$el.html ''
    @collection.each (model) =>
      @$el.append wa.f.render(@template, {model: model})
      return
