class wa.views.EventPage extends Backbone.View
  
  initialize: ->
    @event = wa.global.event
    
    @posts = new wa.collections.Posts
      url: '/posts/event_feed'
      defaultParams:
        event_id: @event.id
    
    @subscribers = @event.get 'subscribers'
    @publishers = @event.get 'publishers'
    
    @postsView = new wa.views.UserTab
      el:        '#posts'
      template:  'post'
      collection: @posts
    
    @subscribersView = new wa.views.UserTab
      el:        '#subscribers'
      template:  'user'
      collection: @subscribers
    
    @publishersView = new wa.views.UserTab
      el:        '#publishers'
      template:  'user'
      collection: @publishers
    
    @map = new wa.views.EventMap
      model: @event
