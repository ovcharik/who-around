class wa.views.EventMap extends Backbone.View
  el: '#map'
  
  initialize: (options) ->
    @model = options.model
    
    @initMap()
  
  initMap: ->
    ll = @model.latlng()
    @gmap = new google.maps.Map @$el[0],
      center: new google.maps.LatLng ll
      zoom: 8
    @marker = new google.maps.Marker
      position: ll
      map: @gmap
