class wa.views.UserMap extends Backbone.View
  el: '#map'
  
  initialize: (options) ->
    @collection = options.collection
    @markers = []
    
    @page = 1
    
    @initMap()
    @bindEvents()
  
  initMap: ->
    @gmap = new google.maps.Map @$el[0],
      center: new google.maps.LatLng 55.174533, 61.426681
      zoom: 8
  
  bindEvents: ->
    @collection.on 'sync', @render, @
  
  render: (collection, data, options) ->
    if options.remove
      @clearMarkers()
    
    _.each @collection.page(@page), (p) =>
      @addMarker p
    
    @page += 1
  
  addMarker: (p) ->
    @markers.push new google.maps.Marker
      position: p.latlng()
      map: @gmap
  
  clearMarkers: ->
    _.each @markers, (m) =>
      m.setMap(null)
