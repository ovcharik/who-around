class wa.views.UserTab extends Backbone.View
  
  initialize: (options) ->
    @el         = options.el
    @template   = options.template
    @collection = options.collection
    
    @scrollStop = true
    @page = 0
    
    @setElement @el
    @bindEvents()
    @load()
  
  bindEvents: ->
    @collection.on 'sync', @render, @
    
    @on 'scroll', @load, @
  
  render: (collection, data, options) ->
    if options.remove
      @$el.html ''
    
    _.each @collection.page(@page), (p, i) =>
      @$el.append wa.f.render(@template, { model: p })
    
    if not data.length or data.length < @collection.perPage
      @scrollOff
    else
      @scrollStop = false
  
  load: ->
    @scrollStop = true
    @page += 1
    @collection.load {page: @page},
      remove: @page == 1
  
  @extend wa.mixins.Callbacks
  @extend wa.mixins.ScrollListener
