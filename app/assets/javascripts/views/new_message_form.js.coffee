class wa.views.NewMessageForm extends wa.views.BaseForm
  
  
  submit: (event) ->
    event.preventDefault()
    
    $buttons = @$el.find('button, input[type=submit]')
    $buttons.prop('disabled', true)
    
    text  = @$el.find('[name="post[text]"]').val()
    event = @$el.find('[name="post[event]"]').val()
    
    navigator.geolocation.getCurrentPosition (position) =>
      location =
        lat: position.coords.latitude
        lng: position.coords.longitude
      
      wa.global.currentUser.createMessage text, location, event,
        success: =>
          $buttons.prop('disabled', false)
          @$el[0].reset()
          alertify.success 'Success!'
        error: =>
          $buttons.prop('disabled', false)
          alertify.error 'Error!'
