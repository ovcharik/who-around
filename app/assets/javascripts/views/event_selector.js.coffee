class wa.views.EventSelector extends Backbone.View
  
  collection: undefined
  template: 'event_option'
  
  events:
    'click .update': 'update'
  
  initialize: ($el) ->
    @$el = $el
    @delegateEvents()
    
    @$select = @$el.find 'select'
    
    load = not @collection?
    @collection ||= new wa.collections.Events
      url: '/around_events'
    @collection.on 'sync', @render, @
    
    if load
      @update()
    else
      @render()
  
  update: ->
    navigator.geolocation.getCurrentPosition (position) =>
      @collection.load
        location: wa.f.formatLocation position.coords
        radius:   10000
  
  render: ->
    console.log @collection
    @$select.html ''
    @$select.append '<option selected>Events</option>'
    @collection.each (event) =>
      @$select.append wa.f.render(@template, {event: event})
