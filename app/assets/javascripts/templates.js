
window.wa.templates['event'] = function(locals){
function html_escape(text) {
    return (text + "").
      replace(/&/g, "&amp;").
      replace(/</g, "&lt;").
      replace(/>/g, "&gt;").
      replace(/\"/g, "&quot;");
  }
with(locals || {}) {
  try {
   var _$output="";var event = model; _$output = _$output  +
"<div class=\"col-sm-4 col-inline\"><div class=\"panel panel-default\"><div class=\"panel-heading\">" + 
"<a href=\"" + html_escape(event.url()) + "\">" + 
event.get('title') + 
"</a></div><div class=\"panel-body\">" + 
event.get('description') + 
"</div></div></div>";
 return _$output;  } catch (e) {
    return "\n<pre class='error'>" + html_escape(e.stack) + "</pre>\n";
  }
}
}


window.wa.templates['event_option'] = function(locals){
function html_escape(text) {
    return (text + "").
      replace(/&/g, "&amp;").
      replace(/</g, "&lt;").
      replace(/>/g, "&gt;").
      replace(/\"/g, "&quot;");
  }
with(locals || {}) {
  try {
   var _$output="<option value=\"" + html_escape(event.id) + "\">" + 
event.get('title') + 
"</option>";
 return _$output;  } catch (e) {
    return "\n<pre class='error'>" + html_escape(e.stack) + "</pre>\n";
  }
}
}


window.wa.templates['post'] = function(locals){
function html_escape(text) {
    return (text + "").
      replace(/&/g, "&amp;").
      replace(/</g, "&lt;").
      replace(/>/g, "&gt;").
      replace(/\"/g, "&quot;");
  }
with(locals || {}) {
  try {
   var _$output="";var post = model;; _$output = _$output  +
"";var user = post.get('user');; _$output = _$output  +
"";var currentUser = currentUser || false;; _$output = _$output  +
"";var subscribed = currentUser && currentUser.subscribed(user);; _$output = _$output  +
"<div class=\"list-group-item clearfix\">" + 
(function () { if ((user)) { return (
"<div class=\"row\"><div class=\"col-xs-2 center\">" + 
"<a href=\"" + html_escape(user.url()) + "\">" + 
"<img src=\"" + html_escape(user.get('avatars').small) + "\" width=\"" + html_escape('100%') + "\" style=\"" + html_escape('min-width: 35px') + "\" />" + 
"</a>" +
(function () { if ((currentUser != user)) { return (
(function () { if ((currentUser && !subscribed)) { return (
"<hr class=\"small-margin\" />" + 
"<a wa-action=\"" + html_escape('read') + "\" wa-type=\"" + html_escape('user') + "\" wa-id=\"" + html_escape(user.id) + "\" class=\"btn btn-default btn-sm\">" + 
"Read</a>"
);} else { return ""; } }).call(this) +
(function () { if ((currentUser && subscribed)) { return (
"<hr class=\"small-margin\" />" + 
"<a wa-action=\"" + html_escape('unread') + "\" wa-type=\"" + html_escape('user') + "\" wa-id=\"" + html_escape(user.id) + "\" class=\"btn btn-success btn-sm\">" + 
"Unread</a>"
);} else { return ""; } }).call(this)
);} else { return ""; } }).call(this) + 
"</div><div class=\"col-xs-10\">" + 
"<a href=\"" + html_escape(user.url()) + "\">" + 
"<b>" + 
user.get('username') + 
"</b></a><br />" +
post.get('text') +
"<hr class=\"small-margin\" /><div class=\"pull-right\">" + 
post.get('created_at') + 
"</div></div></div>"
);} else { return ""; } }).call(this) +
(function () { if (!(user)) { return (
post.get('text')
);} else { return ""; } }).call(this) + 
"</div>";
 return _$output;  } catch (e) {
    return "\n<pre class='error'>" + html_escape(e.stack) + "</pre>\n";
  }
}
}


window.wa.templates['user'] = function(locals){
function html_escape(text) {
    return (text + "").
      replace(/&/g, "&amp;").
      replace(/</g, "&lt;").
      replace(/>/g, "&gt;").
      replace(/\"/g, "&quot;");
  }
with(locals || {}) {
  try {
   var _$output="";var user = model;; _$output = _$output  +
"";var currentUser = currentUser || false;; _$output = _$output  +
"";var subscribed = currentUser && currentUser.subscribed(user);; _$output = _$output  +
"<div class=\"col-sm-4\"><div class=\"panel panel-default\"><div class=\"panel-heading\">" + 
"<a href=\"" + html_escape(user.url()) + "\">" + 
user.get('username') + 
"</a></div><div class=\"panel-body\">" + 
"<a href=\"" + html_escape(user.url()) + "\">" + 
"<img src=\"" + html_escape(user.get('avatars').big) + "\" width=\"" + html_escape('100%') + "\" />" + 
"</a></div><div class=\"panel-footer\"><div class=\"row center\"><div class=\"btn-group\">" + 
"<span title=\"" + html_escape('Posts') + "\" class=\"btn btn-default\">" + 
user.get('post_count') + 
"</span>" + 
"<span title=\"" + html_escape('Subscribers') + "\" class=\"btn btn-default\">" + 
user.get('subscriber_count') + 
"</span>" + 
"<span title=\"" + html_escape('Publishers') + "\" class=\"btn btn-default\">" + 
user.get('publisher_count') + 
"</span></div>" +
(function () { if ((currentUser != user)) { return (
(function () { if ((currentUser && !subscribed)) { return (
"<hr class=\"small-margin\" />" + 
"<a wa-action=\"" + html_escape('read') + "\" wa-type=\"" + html_escape('user') + "\" wa-id=\"" + html_escape(user.id) + "\" class=\"btn btn-default btn-sm\">" + 
"Read</a>"
);} else { return ""; } }).call(this) +
(function () { if ((currentUser && subscribed)) { return (
"<hr class=\"small-margin\" />" + 
"<a wa-action=\"" + html_escape('unread') + "\" wa-type=\"" + html_escape('user') + "\" wa-id=\"" + html_escape(user.id) + "\" class=\"btn btn-success btn-sm\">" + 
"Unread</a>"
);} else { return ""; } }).call(this)
);} else { return ""; } }).call(this) + 
"</div></div></div></div>";
 return _$output;  } catch (e) {
    return "\n<pre class='error'>" + html_escape(e.stack) + "</pre>\n";
  }
}
}
