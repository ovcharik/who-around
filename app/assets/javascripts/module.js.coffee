moduleKeywords = ['extended', 'included']

class wa.Module
  @extend: (obj) ->
    for key, value of obj when key not in moduleKeywords
      @[key] = value
    
    obj.extended?.apply(@)
    this

  @include: (obj) ->
    for key, value of obj when key not in moduleKeywords
      @::[key] = value
    
    obj.included?.apply(@)
    this

_.extend Backbone.RelationalModel, wa.Module
_.extend Backbone.Model,           wa.Module
_.extend Backbone.Collection,      wa.Module
_.extend Backbone.View,            wa.Module
_.extend Backbone.Router,          wa.Module
_.extend Backbone.History,         wa.Module
