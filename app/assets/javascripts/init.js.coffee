$ ->
  # init global vars
  ((p, g) ->
    
    g.signedIn = p.signedIn
    
    if !p.user || !p.currentUser || p.currentUser._id != p.user._id
      g.currentUser = new wa.models.User(p.currentUser) if p.currentUser
    
    g.user  = new wa.models.User  p.user  if p.user
    g.post  = new wa.models.Post  p.post  if p.post
    g.event = new wa.models.Event p.event if p.event
    
  )(wa.proxy, wa.global)
  
  # init models
  wa.models.User.setup()
  wa.models.Post.setup()
  wa.models.Event.setup()
  
  # dom inits
  wa.dominits[fName]() for fName of wa.dominits
  
  # start
  router = new wa.Router
  Backbone.history.start
    pushState: true
