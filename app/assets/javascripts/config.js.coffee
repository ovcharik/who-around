# constants
DEBUG = true

# app
window.wa =
  collections: {}
  models: {}
  views: {}
  templates: {}
  mixins: {}
  f: {}
  proxy: {}
  global: {}
  dominits: {}

# logger
unless DEBUG
  console.debug = ->

# ajax csrf
$.ajaxSetup
  headers:
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
