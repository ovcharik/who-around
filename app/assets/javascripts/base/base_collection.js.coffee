class wa.BaseCollection extends Backbone.Collection
  baseUrl: '/'
  perPage: 20
  
  constructor: (mainOptions, refOptions) ->
    options = mainOptions || refOptions || {}
    
    @rootUrl = options.url || @baseUrl
    @defaultParams = options.defaultParams || {}
    
    @params = {}
    
    super
  
  url: ->
    @params ||= {}
    params = _.extend {}, @defaultParams, @params
    "#{@rootUrl}?" + $.param(params)
  
  load: (params = {}, options) ->
    @params = params
    @fetch(options)
  
  page: (p) ->
    @slice((p - 1) * @perPage, p * @perPage)
