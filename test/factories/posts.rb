# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post do
    text     { FactoryGirl.generate :random_text }
    location { FactoryGirl.generate :location }
    user     { FactoryGirl.generate :random_user }
    event    { FactoryGirl.generate :random_event }
  end
end
