FactoryGirl.define do
  factory :user do
    username              { FactoryGirl.generate :user_name }
    email                 { FactoryGirl.generate :email }
    avatar                { FactoryGirl.generate :avatar }
    password              'password'
    password_confirmation 'password'
  end
end
