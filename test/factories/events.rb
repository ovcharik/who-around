# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    title       { FactoryGirl.generate :event_name }
    description { FactoryGirl.generate :random_text }
    location    { FactoryGirl.generate :location }
    creator     { FactoryGirl.generate :random_user }
  end
end
