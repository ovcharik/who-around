FactoryGirl.define do
  sequence :email do |n|
    Faker::Internet.email
  end
  
  sequence :user_name do |n|
    Faker::Internet.user_name
  end
  
  sequence :client_name do |n|
    Faker::Name.name
  end

  sequence :phone, 10 do |n|
    Faker::PhoneNumber.phone_number
  end
  
  avatars = Dir["#{Rails.root}/test/fixtures/avatars/*.jpg"].map{ |p| File.new(p, 'r') }
  sequence :avatar do |n|
    avatars[n % avatars.length]
  end
  
  sequence :event_name do |n|
    Faker::Company.name
  end
  
  sequence :location do |n|
    [ rand(61.1..61.7), rand(55.0..55.3) ]
  end
  
  sequence :random_user do |n|
    count = User.count
    skip  = (count * rand).to_i
    User.limit(1).skip(skip).first
  end
  
  sequence :random_event do |n|
    count = Event.count
    skip  = (count * rand).to_i
    if rand < 0.7
      Event.limit(1).skip(skip).first
    end
  end
  
  sequence :random_post do |n|
    count = Post.where( :event => nil ).count
    skip  = (count * rand).to_i
    if rand < 0.7
      Post.where( :event => nil ).limit(1).skip(skip).first
    end
  end
  
  sequence(:random_string)  { |n| Faker::Lorem.sentence(2) }
  sequence(:random_text)    { |n| Faker::Lorem.paragraph(2) }
  sequence(:random_address) { Faker::Address.street_address }
end
