module.exports = (grunt) ->
  
  grunt.initConfig
    haml:
      compile:
        options:
          namespace: 'window.wa.templates'
          language: 'js'
          target: 'js'
          includePath: true
          pathRelativeTo: 'app/assets/haml'
        files:
          'app/assets/javascripts/templates.js': ['app/assets/haml/**/*.haml']
    
    watch:
      haml:
        files: 'app/assets/haml/**/*.haml'
        tasks: 'haml'
    
  grunt.loadNpmTasks 'grunt-haml'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  
  grunt.registerTask 'default', ['haml']
  grunt.registerTask 'mywatch', ['default', 'watch']
