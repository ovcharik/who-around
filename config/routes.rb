WhoAround::Application.routes.draw do
  
  devise_for :users, :skip => :all
  devise_scope :user do
    namespace :auth do
      post :sign_in,  :to => 'sessions#create'
      get  :sign_out, :to => 'sessions#destroy'
      
      post :sign_up,  :to => 'registrations#create'
    end
  end
  
  root 'home#index'
  get  'feed', :to => 'home#feed'
  get  'around_posts',  :to => 'home#around_posts'
  get  'around_events', :to => 'home#around_events'
  get  'popular_users', :to => 'home#popular_users'
  
  resources :users, :only => [:index, :show] do
    get 'subscribers', :on => :collection
    get 'publishers',  :on => :collection
    
    get 'subscribed_events', :on => :collection
    get 'published_events', :on => :collection
    
    get 'read',   :on => :collection
    get 'unread', :on => :collection
  end
  
  resources :posts, :only => [:index, :show, :create] do
    get 'feed', :on => :collection
    get 'event_feed', :on => :collection
  end
  
  resources :events, :only => [:show] do
    get 'subscribers', :on => :collection
    get 'publishers',  :on => :collection
    
    get 'read',   :on => :collection
    get 'unread', :on => :collection
  end
  
end
